//
//  SpecialLabel.swift
//  Budgeted
//
//  Created by Jason Okoro on 8/6/20.
//  Copyright © 2020 Divercity. All rights reserved.
//

import UIKit

class SpecialLabel: UILabel {
    // Code init
//    override init(frame: CGRect) {
//        super.init(frame: frame)
//
//        // Set font
//        setFont(to: "Avenir-Heavy", size: .special)
//    }
    
    // Storyboard init
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        // Set font
        setFont(to: .heavy, size: .special)
        
        // Set cornerRadius to regular
        setCornerRadius(size: .special)
        
        // Create border
        createBorder(width: .special, color: UIColor.black.cgColor)
        
        // Shift text 87 pts from the left
//        contentHorizontalAlignment = .left
//        titleEdgeInsets.left = 87

        // Make sure background is white 
        setBackgroundColor(to: UIColor.white.cgColor)
    }
}
