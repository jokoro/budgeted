//
//  RegularButton.swift
//  Budgeted
//
//  Created by Jason Okoro on 8/4/20.
//  Copyright © 2020 Divercity. All rights reserved.
//

import UIKit

class RegularButton: UIButton {
    

    // Code init
//    override init(frame: CGRect) {
//        super.init(frame: frame)
//
//        // Set font
//        setFont(to: "Avenir-Heavy", size: .special)
//    }
    
    // Storyboard init
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        // Set font
        setFont(to: .heavy, size: .regular)
        
        // Set cornerRadius to regular
        setCornerRadius(size: .regular)
        
        // Set height
        setHeight(to: .normal)
    }
}
