//
//  CopyButton.swift
//  Budgeted
//
//  Created by Jason Okoro on 8/21/20.
//  Copyright © 2020 Divercity. All rights reserved.
//

import UIKit

class CopyButton: UIButton {
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        // Set font
        setFont(to: .roman, size: .regular)
        
        // Set cornerRadius to regular
        setCornerRadius(size: .regular)
        
        // Indent content
        contentEdgeInsets = UIEdgeInsets(top: 0, left: 15 , bottom: 0, right: 0)
        
        if let imageWidth = imageView?.frame.width {
            let imageLeftIndent = frame.width - contentEdgeInsets.left - imageWidth - 21
            
            imageEdgeInsets = UIEdgeInsets(top: 0, left: imageLeftIndent , bottom: 0, right: 0)
            
            titleEdgeInsets = UIEdgeInsets(top: 0, left: -imageWidth, bottom: 0, right: 0)
        }
    }
}
