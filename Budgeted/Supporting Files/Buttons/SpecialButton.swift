//
//  SpecialButton.swift
//  Budgeted
//
//  Created by Jason Okoro on 8/4/20.
//  Copyright © 2020 Divercity. All rights reserved.
//

import UIKit

class SpecialButton: UIButton {
    
    // Code init
//    override init(frame: CGRect) {
//        super.init(frame: frame)
//
//        // Set font
//        setFont(to: "Avenir-Heavy", size: .special)
//    }
    
    // Storyboard init
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        // Set text color for normal and when touched
        setTitleColor(.black, for: .normal)
//        setTitleColor(.black, for: .highlighted)
        
        // Set font
        setFont(to: .heavy, size: .special)
        
        // Set cornerRadius
        setCornerRadius(size: .special)
        
        // Set insets
        setInsets()
        
        // Highlight when touched 
        addHighlightingFunctionality()
        
        // Set height
        setHeight(to: .normal)

    }
    
    func setInsets() {
        contentHorizontalAlignment = .left
        
        imageEdgeInsets.left = 21
        titleEdgeInsets.left = contentEdgeInsets.left + imageEdgeInsets.left + 23
    }
    
//    func makeItSocialSignIn() {
//        // Makes sure background is white
//        setBackgroundColor(to: UIColor.white.cgColor)
//
//        // Has border
//        createBorder(width: .special, color: UIColor.black.cgColor)
//    }
}
