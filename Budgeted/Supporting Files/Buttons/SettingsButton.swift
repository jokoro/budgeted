//
//  SettingsButton.swift
//  Budgeted
//
//  Created by Jason Okoro on 8/19/20.
//  Copyright © 2020 Divercity. All rights reserved.
//

import UIKit

class SettingsButton: UIButton {
    private let forwardImageView = UIImageView(image: UIImage(named: "forward"))

    func addForwardButtons() {
        addSubview(forwardImageView)
        
        forwardImageView.translatesAutoresizingMaskIntoConstraints = false

        NSLayoutConstraint(item: forwardImageView, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailing, multiplier: 1, constant: -15).isActive = true
        NSLayoutConstraint(item: forwardImageView, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0).isActive = true
        NSLayoutConstraint(item: forwardImageView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 14).isActive = true
        NSLayoutConstraint(item: forwardImageView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 8).isActive = true
    }
}

class NormalButton: SettingsButton {
    // Init
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setUp()
    }
    
    // Setup
    private func setUp() {
        setBackgroundColor(to: UIColor.white.cgColor)
        addForwardButtons()
        contentEdgeInsets = UIEdgeInsets(top: 0, left: 19 , bottom: 0, right: 0)
    }
}

class EditButton: SettingsButton {
    private var extraTextLabel: UILabel!
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        extraTextLabel = setUpExtraText()
        setUp()
    }
    
    private func setUp() {
        setBackgroundColor(to: UIColor.white.cgColor)
        addForwardButtons()
        titleLabel?.font = UIFont(name: "Avenir-Roman", size: 17)
        contentHorizontalAlignment = .left
        contentEdgeInsets = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 0)
    }

    private func setUpExtraText() -> UILabel {
        let x: CGFloat = frame.origin.x
        let y: CGFloat = frame.origin.y

        let label = UILabel(frame: CGRect(x: x, y: y, width: 0, height: 22))
        label.font = UIFont(name: "Avenir-Heavy", size: 17)

        label.translatesAutoresizingMaskIntoConstraints = false
        addConstraint(NSLayoutConstraint(item: label, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailing, multiplier: 1, constant: -36))
        addConstraint(NSLayoutConstraint(item: label, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0))
        addSubview(label)
        
        return label
    }
    
    func addExtraText(extraText: String?) {
//        guard let extraText = extraText else { return }
        extraTextLabel.text = extraText
    }
}

class ProfileButton: SettingsButton {
    // Properties
    private var extraTextLabel: UILabel!

    // Init
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        extraTextLabel = setUpExtraText()
        setUp()
    }
    
    // Setup
    private func setUp() {
        setBackgroundColor(to: UIColor.white.cgColor)
        addForwardButtons()
        contentEdgeInsets = UIEdgeInsets(top: 0, left: 19 , bottom: 0, right: 0)
    }

    // Setup extra text
    private func setUpExtraText() -> UILabel {
        let extraTextHeight: CGFloat = 21
        let x: CGFloat = contentEdgeInsets.left + imageEdgeInsets.left + (imageView?.frame.width)! + titleEdgeInsets.left
        let y: CGFloat = frame.height/2 + titleEdgeInsets.top + extraTextHeight

        let label: UILabel = UILabel(frame: CGRect(x: x, y: y, width: frame.width - x, height: extraTextHeight))
        label.font = UIFont(name: "Avenir-Roman", size: 16)
        addSubview(label)
        
        return label
    }
    
    // Adding extra text
    func addExtraText(extraText: String?) {
//        guard let extraText = extraText else { return }
        extraTextLabel.text = extraText
    }
}

class ImageButton: SettingsButton {
    let photoImage = UIImage(named: "photo")

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setUp()
    }
    
    private func setUp() {
        setBackgroundColor(to: UIColor.white.cgColor)
        addForwardButtons()
        
        let photoImageView = UIImageView(image: photoImage)
        addSubview(photoImageView)
        
        photoImageView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint(item: photoImageView, attribute: .centerX, relatedBy: .equal, toItem: self, attribute: .centerX, multiplier: 1, constant: 0).isActive = true
        NSLayoutConstraint(item: photoImageView, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0).isActive = true
    }
}

class NoForwardButton: SettingsButton {
    // Init
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setUp()
    }
    
    // Setup
    private func setUp() {
        setBackgroundColor(to: UIColor.white.cgColor)
        let redColor = UIColor(red: 1, green: 0.231, blue: 0.188, alpha: 1)
        setTitleColor(redColor, for: .normal)
        contentEdgeInsets = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 0)
    }
}
