//
//  InAppNotificationViewController.swift
//  Budgeted
//
//  Created by Jason Okoro on 10/28/20.
//  Copyright © 2020 Divercity. All rights reserved.
//

import UIKit

class InAppNotificationView: UIViewController {
    @IBOutlet weak var notificationLabel: UILabel!
    
    class func instanceFromNib() -> InAppNotificationView {
        let view = UINib(nibName: "InAppNotificationViewController", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! InAppNotificationView
        view.setupView()
        return view
     }
    
    fileprivate func setupView() {
        view.layer.cornerRadius = 6
        view.layer.borderWidth = 0.5
        view.layer.borderColor = .init(srgbRed: 0, green: 0, blue: 0, alpha: 1)
        modalPresentationStyle = .currentContext
    }
}
