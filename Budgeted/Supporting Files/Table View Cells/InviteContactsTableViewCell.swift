//
//  InviteContactsTableViewCell.swift
//  Budgeted
//
//  Created by Jason Okoro on 9/2/20.
//  Copyright © 2020 Divercity. All rights reserved.
//

import UIKit
import Contacts

class InviteContactsTableViewCell: UITableViewCell {
    var nameLabel = UILabel()
    var phoneNumberLabel = UILabel()
    var inviteButton = UIButton()
    var vcDelegate: InviteContactsViewController?
    
    func setUp(presentingDelegate: InviteContactsViewController, contact: CNContact) {
        // Set phone number and delegate
        let phoneNumber = contact.phoneNumbers[0].value.stringValue
        vcDelegate = presentingDelegate

        // Edit fonts and text
        // : name label
        nameLabel.font = UIFont(name: "Avenir-Roman", size: 17)
        nameLabel.text = contact.givenName + " " + contact.familyName

        // : phone number label
        phoneNumberLabel.font = UIFont(name: "Avenir-Roman", size: 15)
        phoneNumberLabel.text = phoneNumber
        phoneNumberLabel.textColor = UIColor(red: 0.235, green: 0.235, blue: 0.263, alpha: 0.6)

        // : invite button
        inviteButton.setTitle("Invite", for: .normal)
        inviteButton.setTitleColor(UIColor(red: 0.487, green: 0.631, blue: 0.95, alpha: 1), for: .normal)
        inviteButton.titleLabel?.font = UIFont(name: "Avenir-Heavy", size: 17)

        // Add to subview
        contentView.addSubview(nameLabel)
        contentView.addSubview(phoneNumberLabel)
        contentView.addSubview(inviteButton)

        // Add contraints
        // : name label
        nameLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint(item: nameLabel, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1, constant: 9).isActive = true
        NSLayoutConstraint(item: nameLabel, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1, constant: 16).isActive = true

        // : phone number label
        phoneNumberLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint(item: phoneNumberLabel, attribute: .top, relatedBy: .equal, toItem: nameLabel, attribute: .bottom, multiplier: 1, constant: 0).isActive = true
        NSLayoutConstraint(item: phoneNumberLabel, attribute: .leading, relatedBy: .equal, toItem: nameLabel, attribute: .leading, multiplier: 1, constant: 0).isActive = true

        // : invite button
        inviteButton.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint(item: inviteButton, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0).isActive = true
        NSLayoutConstraint(item: inviteButton, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailing, multiplier: 1, constant: -7).isActive = true

        // Add target to invite button
        inviteButton.addTarget(self, action: #selector(inviteButtonPressed), for: .touchUpInside)
    }
    
    @objc
    func inviteButtonPressed() {
        vcDelegate?.inviteButtonPressed(phoneNumber: phoneNumberLabel.text!)
    }
}
