//
//  Screen.swift
//  Budgeted
//
//  Created by Jason Okoro on 8/4/20.
//  Copyright © 2020 Divercity. All rights reserved.
//

import UIKit

struct Screen {
    static let rect = UIScreen.main.bounds
    static let width = rect.size.width
    static let height = rect.size.height
}
