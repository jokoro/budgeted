//
//  SettingsTextField.swift
//  Budgeted
//
//  Created by Jason Okoro on 8/20/20.
//  Copyright © 2020 Divercity. All rights reserved.
//

import UIKit

class SettingsTextField: UITextField {
    var hasSetup = false

    override func layoutSubviews() {
        super.layoutSubviews()
        if !hasSetup {
            hasSetup = true
            configureBorder()
            setFont()
            align()
            setBackground()
            addDivider()
        }
    }
    
    private func setFont() {
        font = UIFont(name: "Avenir-Roman", size: 17)
        textColor = UIColor.black
    }
    
    private func setBackground() {
        setBackgroundColor(to: UIColor.white.cgColor)
    }
    
    private func align() {
        textAlignment = .left
        leftView = UIView(frame: CGRect(x: 0, y: 0, width: 16, height: 0))
        leftViewMode = .always
    }
    
    private func configureBorder() {
        borderStyle = .none
    }
    
    func shouldNotResign() -> Bool {
        return text == nil || text == ""
    }
}
