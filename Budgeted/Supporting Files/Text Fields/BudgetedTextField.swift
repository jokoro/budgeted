//
//  BudgetedTextField.swift
//  Budgeted
//
//  Created by Jason Okoro on 8/5/20.
//  Copyright © 2020 Divercity. All rights reserved.
//

import UIKit

class BudgetedTextField: UITextField {    
    // Placeholder label
    let placeholderLabel = UILabel(frame: CGRect(x: 16, y: 0, width: 145, height: 22))
    var updatedText = ""

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        _initPlaceholderLabel()
        setCornerRadius(size: .regular)
        setConstraints()
//        addPadding()
        setTextFont()
    }
    
    func _initPlaceholderLabel() {
        placeholderLabel.text = placeholder?.uppercased()
                
        placeholderLabel.textColor = UIColor(red: 0.2, green: 0.2, blue: 0.2, alpha: 1)
        
        // MARK: XCode doesn't have Muli-Regular Font, so using Avenir-Roman instead
        placeholderLabel.font = UIFont(name: "Avenir-Roman", size: 10)        
    }
    
    // Add label when text is added, delete it when none
    func addOrRemovePlaceholderLabel() {
                
        if updatedText != "" {
            addPlaceholderLabel()
        } else {
            removePlaceholderLabel()
        }
    }
    
    func addPlaceholderLabel() {
        addSubview(placeholderLabel)
    }
    func removePlaceholderLabel() {
        placeholderLabel.removeFromSuperview()
    }

    // Edit padding
    let padding = UIEdgeInsets(top: 16.5, left: 16, bottom: 9.5, right: 0);
    let placeholderPadding = UIEdgeInsets(top: 13, left: 15, bottom: 13, right: 0);
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: placeholderPadding)
    }
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
}

// Extend the delegate so that it must give us a stack view to change the alpha of
protocol TextFieldStackViewDelegate: UITextFieldDelegate {
    var minCharacterImageView: UIImageView! { get set }
    var minCaseImageView: UIImageView! { get set }
    var minNumberImageView: UIImageView! { get set }
    var minSpecialImageView: UIImageView! { get set }
    var passwordPredicateStackView: UIStackView! { get set }
    var minCharacters: Int { get }
}

extension BudgetedTextField {
    func setConstraints() {
        // Height
        NSLayoutConstraint(item: self, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: Screen.height/812, constant: 48).isActive = true
    }
    
    func addPadding() {
        let padding = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: 0))
                
        leftView = padding
        leftViewMode = .always
        
        
    }
    
    func setTextFont() {
        font = UIFont(name: "Avenir-Roman", size: 17)
        textColor = UIColor(red: 0.31, green: 0.31, blue: 0.31, alpha: 1)
    }
}

// Validity Functions
extension BudgetedTextField {
    func isValidName() -> Bool{
        return (text?.count)! > 0
    }
    
    func isValidEmail() -> Bool {
        let emailRegEx = "[ ]*[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}[ ]*"
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: text!)
    }    
}
