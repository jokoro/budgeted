//
//  SignUpTextField.swift
//  Budgeted
//
//  Created by Jason Okoro on 8/22/20.
//  Copyright © 2020 Divercity. All rights reserved.
//

import UIKit

class SignUpTextField: BudgetedTextField {
    static var validArray = [false, false, false, false]
    
    // override var passwordRules: UITextInputPasswordRules?
    var signUpDelegate: TextFieldStackViewDelegate?
    var style: SignUpTextField.Style!
    var stackView: UIStackView!
    
    // Flags for Style.password text fields
    var minCharacterRequirementMet = false
    var minCaseRequirementMet = false
    var minNumberRequirementMet = false
    var minSpecialRequirementMet = false

    // Make password requirements visible when passwordTextView is first responder
    override func becomeFirstResponder() -> Bool {
        if window != nil {
            let value = super.becomeFirstResponder()
            
            if style == SignUpTextField.Style.password {  // in password text field so make visible
                text = ""
                updatedText = ""
                updatePasswordPredicates(minCharacters: signUpDelegate?.minCharacters ?? 8)
                updatePasswordPredicateStackView()
                stackView.alpha = 1
            } else {  // not in password text field so make invisible
                stackView.alpha = 0
            }
            return value
        }
        return false
    }
    
    // Make password requirements invisible when passwordTextView is not first responder
    override func resignFirstResponder() -> Bool {
        stackView.alpha = 0
        return super.resignFirstResponder()
    }
    
    func setPasswordDelegate(_ newPasswordDelegate: TextFieldStackViewDelegate) {
        signUpDelegate = newPasswordDelegate
        stackView = signUpDelegate?.passwordPredicateStackView
    }
}

extension SignUpTextField {
    enum Style: Int {
        case firstName = 0
        case lastName = 1
        case email = 2
        case password = 3
    } // SignUpTextField.Style
}

// Validity functions
extension SignUpTextField {
    func isValidPassword() -> Bool{
        return minCharacterRequirementMet && minCaseRequirementMet && minNumberRequirementMet && minSpecialRequirementMet
    }

    func isValid() -> Bool {
        var value = false
        
        if style == Style.firstName || style == Style.lastName {
            value = isValidName()
        } else if style == Style.email {
            value = isValidEmail()
        } else {
            value = isValidPassword()
        }
        
        SignUpTextField.validArray[style.rawValue] = value
        return value
    }
    
    static func isValidAll() -> Bool {
        return !validArray.contains(false)
    }
}

// Specifically for password style
extension SignUpTextField {
    func updatePasswordPredicates(minCharacters: Int) {
        let alphabetLowerCase = "abcdefghijklmnopqrstuvwxyz"
        let alphabetUpperCase = alphabetLowerCase.uppercased()
        let numbers = "0123456789"
        let specialCharacters = " !\"#$%&'()*+,-./:;<=>?@[\\]^_`{|}~"
        
        // Required amounts for password
        var numLower = 0
        var numUpper = 0
        var numNumbers = 0
        var numSpecial = 0
        
        for char in updatedText {
            if alphabetLowerCase.contains(char) {
                numLower += 1
            } else if alphabetUpperCase.contains(char) {
                numUpper += 1
            } else if numbers.contains(char) {
                numNumbers += 1
            } else if specialCharacters.contains(char) {
                numSpecial += 1
            }
        }
        
        minCharacterRequirementMet = updatedText.count >= minCharacters
        minCaseRequirementMet = numLower >= 1 && numUpper >= 1
        minNumberRequirementMet = numNumbers >= 1
        minSpecialRequirementMet = numSpecial >= 1
    }
    
    func updatePasswordPredicateStackView() {
        let minCharacterImageView = (signUpDelegate?.minCharacterImageView)!
        let minCaseImageView = (signUpDelegate?.minCaseImageView)!
        let minNumberImageView = (signUpDelegate?.minNumberImageView)!
        let minSpecialImageView = (signUpDelegate?.minSpecialImageView)!
        
        if minCharacterRequirementMet {
            // Check "Must be.."
            minCharacterImageView.image = UIImage(named: "Check")
        } else {
            // X "Must be.."
            minCharacterImageView.image = UIImage(named: "X")
        }
        
        if minCaseRequirementMet {
            // Check "Must contain..."
            minCaseImageView.image = UIImage(named: "Check")
        } else {
            // X "Must contain..."
            minCaseImageView.image = UIImage(named: "X")
        }
        
        if minNumberRequirementMet {
            // Check "Numeric..."
            minNumberImageView.image = UIImage(named: "Check")
        } else {
            // X "Numeric..."
            minNumberImageView.image = UIImage(named: "X")
        }
        
        if minSpecialRequirementMet {
            // Check "Special..."
            minSpecialImageView.image = UIImage(named: "Check")
        } else {
            // X "Special..."
            minSpecialImageView.image = UIImage(named: "X")
        }
    }
}
