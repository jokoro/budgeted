//
//  ViewExtensions.swift
//  Budgeted
//
//  Created by Jason Okoro on 8/5/20.
//  Copyright © 2020 Divercity. All rights reserved.
//

import UIKit

extension UIView {
    // Colors
    static let blue = CGColor(srgbRed: 0.487, green: 0.631, blue: 0.95, alpha: 1)
}

extension UIView {
    // Lengths
    static let buttonAndLabelHeight = Screen.height * 0.0591133005
    static let buttonAndLabelWidth = Screen.width * 0.914666667
}

extension UIView {
    // Function to set height
    func setHeight(to height: UIView.Height) {
        translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint(item: self, attribute: .height, relatedBy: .equal, toItem: nil
            , attribute: .notAnAttribute, multiplier: 1, constant: height.rawValue).isActive = true
    }
    
    // Function to set cornerRadius
    func setCornerRadius(size: CornerRadius) {
        layer.cornerRadius = size.rawValue
    }
    
    // Function to set background color
    func setBackgroundColor(to color: CGColor) {
        layer.backgroundColor = color
    }
    
    // Adds grey 0.5 pt divider at bottom of view
    func addDivider() {
        let dividerLabel = UILabel()
        dividerLabel.backgroundColor = UIColor(red: 0.235, green: 0.235, blue: 0.263, alpha: 0.29)
        addSubview(dividerLabel)
        dividerLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint(item: dividerLabel, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 0.5).isActive = true
        NSLayoutConstraint(item: dividerLabel, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1, constant: 0).isActive = true
        NSLayoutConstraint(item: dividerLabel, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailing, multiplier: 1, constant: 0).isActive = true
        NSLayoutConstraint(item: dividerLabel, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1, constant: 0).isActive = true
    }
    
    // Function to dim background color
    @objc
    func dimBackgroundColor() {
        if let rgba = layer.backgroundColor?.components {
            let dimmedColor = CGColor(srgbRed: rgba[0]/2, green: rgba[1]/2, blue: rgba[2]/2, alpha: rgba[3])
                
            setBackgroundColor(to: dimmedColor)
        }
    }

    // Function to undim dimmed background color
    @objc
    func undimBackgroundColor() {
        if let rgba = layer.backgroundColor?.components {
            let originalColor = CGColor(srgbRed: rgba[0]*2, green: rgba[1]*2, blue: rgba[2]*2, alpha: rgba[3])
                
            setBackgroundColor(to: originalColor)
        }
    }
    
    // Function to create border
    func createBorder(width: BorderWidth, color: CGColor) {
        layer.borderWidth = width.rawValue
        layer.borderColor = color
    }
    
    //  To highlight entire view
    @objc
    func highlight() {
        // Black layer with opacity
        let highlightLayer = CALayer()
        highlightLayer.frame = CGRect(x: 0, y: 0, width: layer.bounds.size.width, height: layer.bounds.size.height)
        highlightLayer.backgroundColor = UIColor.black.cgColor
        highlightLayer.opacity = 0.5

        // Create an image from the view
        UIGraphicsBeginImageContextWithOptions(bounds.size, false, 0.0)
        if let context = UIGraphicsGetCurrentContext() {
            layer.render(in: context)
        }
        let maskImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        // Create a mask layer for the black layer
        let maskLayer = CALayer()
        maskLayer.contents = maskImage?.cgImage
        maskLayer.frame = highlightLayer.frame

        highlightLayer.mask = maskLayer
        layer.addSublayer(highlightLayer)
    }

//    func unhighlight() {
//        highlightLayer.removeFromSuperlayer()
//        highlightLayer = nil
//    }
}

extension UIView {
    enum Height: CGFloat {
        case normal = 48
    }

    enum Font: String {
        case roman = "Avenir-Roman"
        case heavy = "Avenir-Heavy"
    }

    enum FontSize: CGFloat {
        case regular = 17
        case special = 20
    }

    enum CornerRadius: CGFloat {
        case regular = 8
        case special = 6
    }

    enum BorderWidth: CGFloat {
        case regular = 2
        case special = 1
    }
}

