//
//  NavigationControllerExtensions.swift
//  Budgeted
//
//  Created by Jason Okoro on 8/15/20.
//  Copyright © 2020 Divercity. All rights reserved.
//

import UIKit

extension UINavigationController {
    func popAndPushViewController(_ viewController: UIViewController) {
        popViewController(animated: true) // just a note, if animated set to false, navigationController becomes nil for some reason 🤷🏾‍♂️
        pushViewController(viewController, animated: true)
    }
}
