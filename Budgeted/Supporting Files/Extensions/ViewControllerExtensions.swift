//
//  ViewControllerExtensions.swift
//  Budgeted
//
//  Created by Jason Okoro on 8/4/20.
//  Copyright © 2020 Divercity. All rights reserved.
//

import UIKit

// animations
extension UIViewController {
    func standardTransition(_ fromDirection: CATransitionSubtype) -> CATransition {
        let transition = CATransition()
        transition.duration = 0.5  // how long transition lasts
        transition.type = .push  // what transistion looks like
        transition.subtype = fromDirection  // direction next view comes from
        transition.timingFunction = CAMediaTimingFunction(name:CAMediaTimingFunctionName.easeInEaseOut)  // timing, ex: here it's slow then fast then slow
        return transition
    }
}

// sizing/constraints
extension UIViewController {
    func scaleToHeight(scale: CGFloat) -> CGFloat {
        return Screen.height * scale
    }
    
    func screenToptoSubViewTopConstraint(subView: UIView, dist: CGFloat) -> NSLayoutConstraint {
        let topConstraint = NSLayoutConstraint(item: subView, attribute: .top, relatedBy: .equal, toItem: view, attribute: .top, multiplier: 1, constant: dist)
        return topConstraint
    }

    func screenLefttoSubViewLeftConstraint(subView: UIView, dist: CGFloat) -> NSLayoutConstraint {
        let leftConstraint = NSLayoutConstraint(item: subView, attribute: .left, relatedBy: .equal, toItem: view, attribute: .left, multiplier: 1, constant: dist)
        return leftConstraint
    }

    func screenRighttoSubViewRightConstraint(subView: UIView, dist: CGFloat) -> NSLayoutConstraint {
        let rightConstraint = NSLayoutConstraint(item: subView, attribute: .right, relatedBy: .equal, toItem: view, attribute: .right, multiplier: 1, constant: dist)
        return rightConstraint
    }
    
    func screenBottomtoSubViewBottomConstraint(subView: UIView, dist: CGFloat) -> NSLayoutConstraint {
        let bottomConstraint = NSLayoutConstraint(item: subView, attribute: .bottom, relatedBy: .equal, toItem: view, attribute: .bottom, multiplier: 1, constant: dist)
        return bottomConstraint
    }
    
    func heightConstraint(subView: UIView, subViewHeight: CGFloat) -> NSLayoutConstraint {
        let heightConstraint = NSLayoutConstraint(item: subView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: subViewHeight)
        return heightConstraint
    }
    
    func widthConstraint(subView: UIView, subViewWidth: CGFloat) -> NSLayoutConstraint {
        let widthConstraint = NSLayoutConstraint(item: subView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: subViewWidth)
        return widthConstraint
    }

    func goToViewController(_ fromDirection: CATransitionSubtype, viewController: UIViewController) {
        
        // Debug
        print("* \(view.window?.rootViewController == self)")
        print("* self: \(self)")
        print("* viewController: \(viewController)")
        print("*")
        print("* before: presenting: \(String(describing: presentingViewController))\tpresented: \(String(describing: presentedViewController))")
        
        // Create transition
//        let transition = standardTransition(.fromRight)
        
        // Add transition to view
        view.window?.layer.add(standardTransition(fromDirection), forKey: kCATransition)
        
        // So view controller appears full screen
        viewController.modalPresentationStyle = .fullScreen
        
        // So don't get error >:(
        getStartedViewController.dismiss(animated: false)
        
        // Debug
        print("* after dismiss: presenting: \(String(describing: presentingViewController))\tpresented: \(String(describing: presentedViewController))")

        // Present SignUpViewController
        getStartedViewController.present(viewController, animated: false)
        
        // Debug
        print("* after present: presenting: \(String(describing: presentingViewController))\tpresented: \(String(describing: presentedViewController))")
        print("*")
        print("*")

    }
}
