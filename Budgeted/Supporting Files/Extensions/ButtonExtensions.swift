//
//  ButtonExtensions.swift
//  Budgeted
//
//  Created by Jason Okoro on 8/6/20.
//  Copyright © 2020 Divercity. All rights reserved.
//

import UIKit

extension UIButton {
    // Function to set font
    func setFont(to name: UIView.Font, size: FontSize) {
        titleLabel?.font = UIFont(name: name.rawValue, size: size.rawValue)
    }
    
    // Debug
    func testFont() {
        titleLabel?.font = UIFont(name: "Zapfino", size: 30)
    }
    
    // Function to set text color
    func setTextColor(to color: UIColor) {
        titleLabel?.textColor = color
    }
    
    // Add highlighting functionality so highlight when touched
    func addHighlightingFunctionality() {
        adjustsImageWhenHighlighted = false
        addTarget(self, action: #selector(dimBackgroundColor), for: [.touchDown, .touchDragEnter])
        addTarget(self, action: #selector(undimBackgroundColor), for: [.touchUpInside, .touchDragExit])
    }
}
