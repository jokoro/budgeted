//
//  StringExtentions.swift
//  Budgeted
//
//  Created by Jason Okoro on 8/10/20.
//  Copyright © 2020 Divercity. All rights reserved.
//

import UIKit

extension String {
    
    // Just in case ranges other than NSRange, Int range will be default
    func stringRange(_ range: Range<Int>) -> Range<String.Index> {
        
        let startInt = range.startIndex
        let endInt = range.endIndex
        
        let stringIndexStart = index(startIndex, offsetBy: startInt)
        let stringIndexEnd = index(startIndex, offsetBy: endInt)
        
        return stringIndexStart..<stringIndexEnd
        
    }

    // Again, separating this out just in case
    func stringRange(_ range: NSRange) -> Range<String.Index> {
        let startInt = range.location
        let endInt = range.location + range.length
        return stringRange(startInt..<endInt)
    }
    
    // This guarantees that the range we are converting (from NSRange to String.Index range) is the range we are removing
    mutating func removeSubrange(_ range: NSRange) {
        removeSubrange(stringRange(range))
    }
    
    mutating func insert(contentsOf newElements: String, at i: Int) {
        insert(contentsOf: newElements, at: index(startIndex, offsetBy: i))
    }

}
