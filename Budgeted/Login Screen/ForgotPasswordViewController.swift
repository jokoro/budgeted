//
//  ForgotPasswordViewController.swift
//  Budgeted
//
//  Created by Jason Okoro on 8/23/20.
//  Copyright © 2020 Divercity. All rights reserved.
//

import UIKit

class ForgotPasswordViewController: UIViewController {

    var canChangePage = false
    @IBOutlet weak var emailTextField: LogInTextField!
    @IBOutlet weak var sendEmailButton: RegularButton!
    @IBOutlet weak var sendEmailBottomConstraint: NSLayoutConstraint!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        emailTextField.becomeFirstResponder()
        emailTextField.delegate = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
    }
    
    @IBAction func sendEmailButtonPressed(_ sender: Any) {
        textFieldShouldReturn(emailTextField)
    }
    
}

extension ForgotPasswordViewController {
    @objc
    func keyboardWillChange(notification: NSNotification) {
        if let info = notification.userInfo {
            let rect: CGRect = info[UIResponder.keyboardFrameEndUserInfoKey] as! CGRect
            
            // not really needed here or in animations? but adding cus CodeWithChris youtuber did it
            self.view.layoutIfNeeded()
            
            // withDuration and delay aren't working for some reason?
            if notification.name == UIResponder.keyboardWillShowNotification {
                UIView.animate(
                    withDuration: 0.25,
                    delay: 0,
                    options: .curveEaseInOut,
                    animations: {
                        self.view.layoutIfNeeded()
                        // change view height for smooth animation
                        // change constraint to fix in place
                        self.sendEmailButton.frame.origin.y = self.view.frame.height - self.sendEmailButton.frame.height - rect.height - 8
                        self.sendEmailBottomConstraint.constant = rect.height + 8
                })
            } else if notification.name == UIResponder.keyboardWillHideNotification {
                UIView.animate(
                    withDuration: 0.25,
                    delay: 0,
                    options: .curveEaseInOut,
                    animations: {
                        self.view.layoutIfNeeded()
                        self.sendEmailButton.frame.origin.y = self.view.frame.height - self.sendEmailButton.frame.height - 34
                        self.sendEmailBottomConstraint.constant = 34
                })
            }
        }
    }
}

extension ForgotPasswordViewController: UITextFieldDelegate {
    
    // Calls whenever text field changes
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let textField = textField as! LogInTextField
        
        textField.updatedText = textField.text ?? ""
        
        // Create and store update text
        textField.updatedText.removeSubrange(range)
        textField.updatedText.insert(contentsOf: string, at: range.location)

        textField.addOrRemovePlaceholderLabel()
        
        return true
    }
    
    // Go to next text field or next page after hitting return
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let textField = textField as! LogInTextField
        
        if textField.isValidEmail() {
            // Go to next page!
            currentUser.setEmail(email: emailTextField.text)
//            currentUser.sendPasswordResetEmail()
            performSegue(withIdentifier: "sendToCheckEmailSegue", sender: self)
        } else {
            let alert = UIAlertController(title: "Invalid Email", message: "Please enter a vlaid email to continue.", preferredStyle: .alert)
            alert.addAction(            UIAlertAction(title: "OK", style: .default))
            present(alert, animated: true)
        }
        
        return true
    }
}
