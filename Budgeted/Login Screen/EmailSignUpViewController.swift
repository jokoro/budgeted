//
//  EmailSignUpViewController.swift
//  Budgeted
//
//  Created by Jason Okoro on 8/5/20.
//  Copyright © 2020 Divercity. All rights reserved.
//

import UIKit
class EmailSignUpViewController: GeneralLoginViewController, TextFieldStackViewDelegate {
    
    // Outlet variables
    @IBOutlet weak var firstNameTextField: SignUpTextField!
    @IBOutlet weak var lastNameTextField: SignUpTextField!
    @IBOutlet weak var emailTextField: SignUpTextField!
    @IBOutlet weak var passwordTextField: SignUpTextField!
    @IBOutlet weak var textFieldStackView: UIStackView!
    
    @IBOutlet weak var minCharacterImageView: UIImageView!
    @IBOutlet weak var minCaseImageView: UIImageView!
    @IBOutlet weak var minNumberImageView: UIImageView!
    @IBOutlet weak var minSpecialImageView: UIImageView!
    @IBOutlet weak var minCharacterLabel: UILabel!
    @IBOutlet weak var passwordPredicateStackView: UIStackView!
    
    @IBOutlet weak var signUpButton: RegularButton!
    
    @IBOutlet weak var signUpToBottomConstraint: NSLayoutConstraint!
    
    @IBSegueAction func emailLoginSegue(_ coder: NSCoder) -> EmailLogInViewController? {
        return EmailLogInViewController(coder: coder)
    }
    
    // Non-outlet variables
    // : Text fields
    var textFieldArray: [SignUpTextField]!

    // : Password requirements
    var password = ""  // TODO: store in Budgeted User at some point
    let minCharacters = 8
    var minCharacterRequirementMet = false
    var minCaseRequirementMet = false
    var minNumberRequirementMet = false
    var minSpecialRequirementMet = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Text field alignments
        emailTextField.drawPlaceholder(in: CGRect(origin: CGPoint(x: 20, y: 0), size: CGSize(width: 300, height: 40)))
        
        // Set up text fields
        textFieldArray = [firstNameTextField, lastNameTextField, emailTextField, passwordTextField]
        
        firstNameTextField.style = SignUpTextField.Style.firstName
        lastNameTextField.style = SignUpTextField.Style.lastName
        emailTextField.style = SignUpTextField.Style.email
        passwordTextField.style = SignUpTextField.Style.password
        
        for textField in textFieldArray {
            textField.delegate = self
            textField.setPasswordDelegate(self)
        }

        // Update min characters for password
        let xIndex: String.Index = (minCharacterLabel.text?.firstIndex(of: "x"))!
        let afterXIndex = (minCharacterLabel.text?.index(after: xIndex))!
        let range = xIndex..<afterXIndex
        minCharacterLabel.text?.replaceSubrange(range, with: "\(minCharacters)")
                
        // Replace back button to display warning
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "back"), style: .plain, target: self, action: #selector(alertAboutDiscardingSignUpInfo))
        navigationItem.leftBarButtonItem?.imageInsets = UIEdgeInsets(top: 0, left: -8, bottom: 0, right: 0)
        
        // Add notifications for constraining sign up button to key board
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillChange(notification:)),
            name: UIResponder.keyboardWillShowNotification,
            object: nil)
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillChange(notification:)),
            name: UIResponder.keyboardWillHideNotification,
            object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        // Start editing first name
        firstNameTextField.becomeFirstResponder()
    }
    
    @objc
    func keyboardWillChange(notification: NSNotification) {
        if let info = notification.userInfo {
            let rect: CGRect = info[UIResponder.keyboardFrameEndUserInfoKey] as! CGRect
            
            // not really needed here or in animations? but adding cus CodeWithChris youtuber did it
            self.view.layoutIfNeeded()
            
            // withDuration and delay aren't working for some reason?
            if notification.name == UIResponder.keyboardWillShowNotification {
                UIView.animate(
                    withDuration: 0.25,
                    delay: 0,
                    options: .curveEaseInOut,
                    animations: {
                        self.view.layoutIfNeeded()
                        // change view height for smooth animation
                        // change constraint to fix in place
                        self.signUpButton.frame.origin.y = self.view.frame.height - self.signUpButton.frame.height - rect.height - 8
                        self.signUpToBottomConstraint.constant = rect.height + 8
                })
            } else if notification.name == UIResponder.keyboardWillHideNotification {
                UIView.animate(
                    withDuration: 0.25,
                    delay: 0,
                    options: .curveEaseInOut,
                    animations: {
                        self.view.layoutIfNeeded()
                        self.signUpButton.frame.origin.y = self.view.frame.height - self.signUpButton.frame.height - 34
                        self.signUpToBottomConstraint.constant = 34
                })
            }
        }
    }

    // If any text in any text field in this view controller, alert about going back since text will be losAboutt
    @objc
    func alertAboutDiscardingSignUpInfo() {
        var text = ""
        for textField in textFieldArray {
            text += textField.text ?? ""
        }
        
        if text.count > 0 {
            
            // Create the alert
            let alert = UIAlertController(
                title: "Are you sure?",
                message: "If you go back now, your info will be discarded.",
                preferredStyle: .alert
            )

            // Add an action button, go back if "Go Back" clicked
            alert.addAction(UIAlertAction(
                title: "Go Back",
                style: .default,
                handler: {_ in
                    self.navigationController?.popViewController(animated: true)
            }))
            
            // Add cancel button, stay if "Stay" clicked
            alert.addAction(UIAlertAction(title: "Stay", style: .cancel))
            
            // Show the alert
            present(alert, animated: true)
            
        } else {
            
            // Go back if no info entered to discard 
            navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func signUpButtonPressed(_ sender: Any) {
        alertOrGoToNextPage()
    }
}

extension EmailSignUpViewController: UITextFieldDelegate {
    
    // Calls whenever text field changes
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let textField = textField as! SignUpTextField
        
        textField.updatedText = textField.text ?? ""
        // Remove range and insert string
        textField.updatedText.removeSubrange(range)
        textField.updatedText.insert(contentsOf: string, at: range.location)

        textField.addOrRemovePlaceholderLabel()
        
        if textField.style == SignUpTextField.Style.password {
                        
            textField.updatePasswordPredicates(minCharacters: minCharacters)
            textField.updatePasswordPredicateStackView()
        }
        return true
    }
    
    // Go to next text field or next page after hitting return
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let textField = textField as! SignUpTextField
        let textFieldArrayIndex = textFieldArray.firstIndex(of: textField)
        
//        if textField.isValid() {
        if textFieldArrayIndex == textFieldArray.count - 1 {
            alertOrGoToNextPage()
        } else {
                // Go to next text field
                textFieldArray[textFieldArrayIndex! + 1].becomeFirstResponder()
            }
//        } else {
//            let placeholder = (textField?.placeholder)!
//            switchingPagesAlert(title: "Invalid \(placeholder)", message: "Please edit \(placeholder) before continuing.", actionTitle: "OK")
//        }
        
        return true
    }
}

// Next page logic
extension EmailSignUpViewController {
    func alertOrGoToNextPage() {
        var shouldGoToNextPage = true
        for textField in textFieldArray {
            if !textField.isValid() {
                // Debug
                logMessage(textField.text ?? "nil")

                let placeholder = (textField.placeholder)!
                switchingPagesAlert(title: "Invalid \(placeholder)", message: "Please edit \(placeholder) before continuing.", actionTitle: "OK")
                
                shouldGoToNextPage = false
                break
            }
        }
        if shouldGoToNextPage {
            // Go to home page!
            
            // Add name, email, and password
            let firstName = firstNameTextField.text!
            let lastName = lastNameTextField.text!
            let email = emailTextField.text!
            let password = passwordTextField.text!
            
            // Register user to backend
            signUpDelegate = self
            currentUser.registerUser(firstName: firstName, lastName: lastName, email: email, password: password)
            
            // Debug
            print("* Go to home page!")
        }
    }
}

extension EmailSignUpViewController: BudgetedUserSignUpDelegate {
    func alertAndLoginOrSendToHomeScreen(isValidUser: Bool) {
        DispatchQueue.main.async {
            if isValidUser {
                // Send to home screen
                self.performSegue(withIdentifier: "EmailSignUpToHomeSegue", sender: self)
            } else {
                // Alert
                let alert = UIAlertController(title: "User already exists", message: "Click OK to login.", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default) { (action) in
                    self.performSegue(withIdentifier: "EmailSignUptoEmailLoginSegue", sender: self)
                })
                self.present(alert, animated: true)
            }
        }
    }
}
