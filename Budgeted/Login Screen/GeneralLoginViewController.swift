//
//  GeneralLoginViewController.swift
//  Budgeted
//
//  Created by Jason Okoro on 8/14/20.
//  Copyright © 2020 Divercity. All rights reserved.
//

import UIKit

class GeneralLoginViewController: UIViewController {
    let backImage = UIImage(named: "back")
    var shouldSwitchPage: Bool!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Give navigationItem an appearance to edit
        navigationItem.standardAppearance = UINavigationBarAppearance()
        
        // Make nav bar transparent, no blur
        navigationItem.standardAppearance?.configureWithTransparentBackground()

        navigationItem.standardAppearance?.backgroundEffect = nil
        
        // Customize default back button image
        navigationItem.standardAppearance?.setBackIndicatorImage(backImage, transitionMaskImage: backImage)
        
        // Default back buttons will have no text
        navigationItem.backBarButtonItem = UIBarButtonItem(title: nil, style: .plain, target: nil, action: nil)
    }
}

// Alert function
extension GeneralLoginViewController {
    func switchingPagesAlert(title: String, message: String, actionTitle: String, cancelTitle: String? = nil) {
        
        // create the alert
        let alert = UIAlertController(
            title: title,
            message: message,
            preferredStyle: .alert
        )

        // add an action (button)
        alert.addAction(UIAlertAction(
            title: actionTitle,
            style: .default
        ))
        
        // Add cancel button, if specified
        if let cancelTitle = cancelTitle {
            alert.addAction(UIAlertAction(
                title: cancelTitle,
                style: .cancel
            ))
        }

        // show the alert
        present(alert, animated: true)
    }
}
