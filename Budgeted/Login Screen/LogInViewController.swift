//
//  LogInViewController.swift
//  Budgeted
//
//  Created by Jason Okoro on 8/5/20.
//  Copyright © 2020 Divercity. All rights reserved.
//


import UIKit
import GoogleSignIn
import FBSDKLoginKit
import AuthenticationServices

class LogInViewController: GeneralLoginViewController {
    
    // Log in buttons
    @IBOutlet weak var appleLogInButton: SpecialButton!
    @IBOutlet weak var googleLogInButton: SpecialButton!
    @IBOutlet weak var facebookLogInButton: SpecialButton!

    // Functions
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set up buttons
        // MARK: Apple
        setUpAppleSignInButton()
        // MARK: Facebook
        setUpFacebookSignInButton()
        // MARK: Google
        setUpGoogleSignInButton()
    }
    
    @IBAction func signUpButtonPressed(_ sender: Any) {
        navigationController?.popAndPushViewController(signUpViewController)
    }

    @IBAction func googleSignInButtonPressed(_ sender: Any) {
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().signIn()
    }
}


// Sign in button configurations
extension LogInViewController {
    func setUpAppleSignInButton() {
        appleLogInButton.addTarget(self, action: #selector(handleAppleLoginButtonPress), for: .touchUpInside)
    }
    
    func setUpGoogleSignInButton() {
        GIDSignIn.sharedInstance()?.presentingViewController = self
        GIDSignIn.sharedInstance()?.restorePreviousSignIn()
    }
    
    func setUpFacebookSignInButton() {
        // Add sign in button and permissions for Facebook
        facebookLogInButton.addTarget(self, action: #selector(loginButtonClicked), for: .touchUpInside)
    }

    @objc func handleAppleLoginButtonPress() {
        let appleIDProvider = ASAuthorizationAppleIDProvider()
        let request = appleIDProvider.createRequest()
        request.requestedScopes = [.fullName, .email]
        
        let authorizationController = ASAuthorizationController(authorizationRequests: [request])
        authorizationController.delegate = self
        authorizationController.presentationContextProvider = self
        authorizationController.performRequests()
    }
    
    @objc
    func loginButtonClicked() {
        let login = LoginManager()
        login.logIn(
            permissions: ["public_profile", "email"],
            from: self,
            handler: { result, error in
                if error != nil {
                    // Debug
                    print("Process error")
                } else if result?.isCancelled ?? false {
                    // Debug
                    print("Cancelled")
                } else {
                    // Debug
                    print("Logged in")
                    
                    // MARK: Facebook backend
                    GraphRequest(graphPath: "me", parameters: ["fields": "name, email, first_name, last_name"]).start(completionHandler: { connection, result, error in
                        if error == nil {
                            if let result = result as? NSDictionary {
                                let name = result["name"]! as! String
                                let firstName = result["first_name"]! as! String
                                let lastName = result["last_name"]! as! String
                                let email = result["email"]! as! String
                                print("facebook: ", name, email, firstName, lastName)
                                
                                // MARK: Store FB user.
                                self.saveUserInDataBase([name, email])
                            }
                        }
                    })

                }
            })
    }
    
    // TODO: Save user data in backend
    func saveUserInDataBase(_ userIdentifiers: [String]) {

    }
}


// Required sign in button protocols as extensions
// : Apple # 1
extension LogInViewController: ASAuthorizationControllerDelegate {

    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        
        // authorization.credential can come in 1 of 2 different forms
        switch authorization.credential {
        case let appleIDCredential as ASAuthorizationAppleIDCredential:
            
            // MARK: Apple backend
            let userIdentifier = appleIDCredential.user
            print("userIdentifier: \(userIdentifier)")
            
            if let fullName = appleIDCredential.fullName {
                print("fullName: \(fullName)")
            }
            
            if let email = appleIDCredential.email {
                print("email: \(email)")
            }

            // MARK: Store Apple user.
            self.saveUserInDataBase([userIdentifier])
            

        case let passwordCredential as ASPasswordCredential:
        
            // Sign in using an existing iCloud Keychain credential.
            let username = passwordCredential.user
            let password = passwordCredential.password
            
            // see if/when we get this information
            print("username: \(username), password: \(password)")
            
        default:
            break
        }
    }
    
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        // Handle error.
        print("There was an error with Apple Sign in: \(error).")
    }
}

// : Apple # 2
extension LogInViewController: ASAuthorizationControllerPresentationContextProviding {
    public func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return self.view.window!
    }
}

// : Google # 1
// Google backend
extension LogInViewController: GIDSignInDelegate {
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if (error == nil) {
            // Perform any operations on signed in user here.
            let userId = user.userID                  // For client-side use only!
            let idToken = user.authentication.idToken // Safe to send to the server
            let fullName = user.profile.name
            let givenName = user.profile.givenName
            let familyName = user.profile.familyName
            let email = user.profile.email
            
            // Debug
            print("google: \nuserId:", userId ?? "no userId", "\n", idToken ?? "no idToken", "\n", fullName ?? "no fullName", "\n", givenName ?? "no givenName", "\n", familyName ?? "no familyName", "\n", email ?? "no email")
            
        } else {
            // Debug
            print("google error: \(error!)")
        }
    }
}
