//
//  ResetPasswordViewController.swift
//  Budgeted
//
//  Created by Jason Okoro on 9/7/20.
//  Copyright © 2020 Divercity. All rights reserved.
//

import UIKit

class ResetPasswordViewController: UIViewController {

    @IBOutlet weak var passwordTextField: LogInTextField!
    @IBOutlet weak var confirmPasswordTextField: LogInTextField!
    @IBOutlet weak var resetPasswordButton: RegularButton!
    @IBOutlet weak var resetPasswordButtonConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        passwordTextField.delegate = self
        confirmPasswordTextField.delegate = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)

    }
}

extension ResetPasswordViewController {
    @objc
    func keyboardWillChange(notification: NSNotification) {
        if let info = notification.userInfo {
            let rect: CGRect = info[UIResponder.keyboardFrameEndUserInfoKey] as! CGRect
            
            // not really needed here or in animations? but adding cus CodeWithChris youtuber did it
            self.view.layoutIfNeeded()
            
            // withDuration and delay aren't working for some reason?
            if notification.name == UIResponder.keyboardWillShowNotification {
                UIView.animate(
                    withDuration: 0.25,
                    delay: 0,
                    options: .curveEaseInOut,
                    animations: {
                        self.view.layoutIfNeeded()
                        // change view height for smooth animation
                        // change constraint to fix in place
                        self.resetPasswordButton.frame.origin.y = self.view.frame.height - self.resetPasswordButton.frame.height - rect.height - 8
                        self.resetPasswordButtonConstraint.constant = rect.height + 8
                })
            } else if notification.name == UIResponder.keyboardWillHideNotification {
                UIView.animate(
                    withDuration: 0.25,
                    delay: 0,
                    options: .curveEaseInOut,
                    animations: {
                        self.view.layoutIfNeeded()
                        self.resetPasswordButton.frame.origin.y = self.view.frame.height - self.resetPasswordButton.frame.height - 34
                        self.resetPasswordButtonConstraint.constant = 34
                })
            }
        }
    }
}

extension ResetPasswordViewController: UITextFieldDelegate {
    // Calls whenever text field changes
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let textField = textField as? LogInTextField {
        
            textField.updatedText = textField.text ?? ""
            
            // Create and store update text
            textField.updatedText.removeSubrange(range)
            textField.updatedText.insert(contentsOf: string, at: range.location)

            textField.addOrRemovePlaceholderLabel()
        }
        return true
    }
}
