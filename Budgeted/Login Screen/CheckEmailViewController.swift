//
//  CheckEmailViewController.swift
//  Budgeted
//
//  Created by Jason Okoro on 8/23/20.
//  Copyright © 2020 Divercity. All rights reserved.
//
/*
UIActivity.ActivityType.postToFacebook,
UIActivity.ActivityType.postToTwitter,
UIActivity.ActivityType.postToWeibo,
UIActivity.ActivityType.message,
UIActivity.ActivityType.mail,
UIActivity.ActivityType.print,
UIActivity.ActivityType.copyToPasteboard,
UIActivity.ActivityType.assignToContact,
UIActivity.ActivityType.saveToCameraRoll,
UIActivity.ActivityType.addToReadingList,
UIActivity.ActivityType.postToFlickr,
UIActivity.ActivityType.postToVimeo,
UIActivity.ActivityType.postToTencentWeibo,
UIActivity.ActivityType.airDrop,
UIActivity.ActivityType.openInIBooks,
UIActivity.ActivityType.markupAsPDF
*/

import UIKit
import MessageUI

class CheckEmailViewController: UIViewController {
    var chooseEmailActionSheet: UIAlertController?

    override func viewDidLoad() {
        super.viewDidLoad()
        chooseEmailActionSheet = setupChooseEmailActionSheet()
    }
    
    @IBAction func checkEmailPressed(_ sender: Any) {
        // Debug
        print("* Check email!")
        
        present(chooseEmailActionSheet!, animated: true)
        
//        let mailURLs = ["googlegmail://", "message://", "ms-outlook://", "ymail://", "readdle-spark://"]
//
//        // Open Mail if installed, else try from in given order mailURLs
//        if MFMailComposeViewController.canSendMail() {
//            UIApplication.shared.open(URL(string: "message://")!)
//        } else {
//            mailUrlLoop: for mailURLString in mailURLs {
//                let mailURL = URL(string: mailURLString)!
//                logMessage(mailURLString, UIApplication.shared.canOpenURL(mailURL))
//
//                UIApplication.shared.open(mailURL, options: [:])
//
//                if UIApplication.shared.canOpenURL(mailURL) {
//                    UIApplication.shared.open(mailURL, options: [:])
//                    break mailUrlLoop
//                }
//            }
//        }
    }
}

extension CheckEmailViewController: MFMailComposeViewControllerDelegate {
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
}

protocol ChooseEmailActionSheetPresenter {
   var chooseEmailActionSheet: UIAlertController? { get }
   func setupChooseEmailActionSheet(withTitle title: String?) -> UIAlertController
    
}

extension ChooseEmailActionSheetPresenter {
    fileprivate func openAction(withURL: EmailUrl, andTitleActionTitle: String) -> UIAlertAction? {
        guard let url = URL(string: withURL.rawValue), UIApplication.shared.canOpenURL(url)
            else {
            return nil
        }

        // Only show Apple Mail if downloaded
//        if withURL == .mail, !MFMailComposeViewController.canSendMail() {
//            return nil
//        }

        let action = UIAlertAction(title: andTitleActionTitle, style: .default) { (action) in
            UIApplication.shared.open(url, options: [:])
        }
        
        return action
    }

    func setupChooseEmailActionSheet(withTitle title: String? = "Choose email") -> UIAlertController {
        let emailActionSheet = UIAlertController(title: title, message: nil, preferredStyle: .actionSheet)
        emailActionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))

        for email in EmailUrl.allCases {
            if let action = openAction(withURL: email, andTitleActionTitle: "\(email)".capitalized) {
                 emailActionSheet.addAction(action)
            }
        }
        
        return emailActionSheet
    }
}

extension CheckEmailViewController: ChooseEmailActionSheetPresenter {}

enum EmailUrl: String, CaseIterable {
    case mail = "message://"
    case gmail = "googlegmail://"
    case outlook = "ms-outlook://"
    case dispatch = "x-dispatch:///"
    case airmail = "airmail://"
    case polymail = "polymail://"
    case protonMail = "protonmail://"
    case spark = "readdle-spark://"
    case edison = "edomail://"
}

