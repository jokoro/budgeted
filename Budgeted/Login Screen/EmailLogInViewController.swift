//
//  EmailLogInViewController.swift
//  Budgeted
//
//  Created by Jason Okoro on 8/5/20.
//  Copyright © 2020 Divercity. All rights reserved.
//

import UIKit

class EmailLogInViewController: GeneralLoginViewController {
    
    // Outlet variables
    @IBOutlet weak var emailTextField: LogInTextField!
    @IBOutlet weak var passwordTextField: LogInTextField!
    @IBOutlet weak var logInButton: RegularButton!
    
    @IBOutlet weak var logInToBottomConstraint: NSLayoutConstraint!

    // Non-outlet variables
    // : Text fields
    var textFieldArray: [LogInTextField]!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set up text fields
        textFieldArray = [emailTextField, passwordTextField]

        // Set self to delegate for all text fields
        for textField in textFieldArray {
            textField.delegate = self
        }
        
        // Add notifications for constraining sign up button to key board
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillChange(notification:)),
            name: UIResponder.keyboardWillShowNotification,
            object: nil)
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillChange(notification:)),
            name: UIResponder.keyboardWillHideNotification,
            object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        // Start editing first name
        emailTextField.becomeFirstResponder()
    }
    
    @IBAction func logInButtonPressed(_ sender: Any) {
        goToHomePage()
    }
    
    func goToHomePage() {
        // Add name, email, and password
        currentUser.setEmail(email: emailTextField.text)
        currentUser.setPassword(password: passwordTextField.text)
        
        // Login user to backend
        logInDelegate = self
        currentUser.loginUser()
    }
    
    @objc
    func keyboardWillChange(notification: NSNotification) {
        if let info = notification.userInfo {
            let rect: CGRect = info[UIResponder.keyboardFrameEndUserInfoKey] as! CGRect
            
            // not really needed here or in animations? but adding cus CodeWithChris youtuber did it
            self.view.layoutIfNeeded()
            
            // withDuration and delay aren't working for some reason?
            if notification.name == UIResponder.keyboardWillShowNotification {
                UIView.animate(
                    withDuration: 0.25,
                    delay: 0,
                    options: .curveEaseInOut,
                    animations: {
                        self.view.layoutIfNeeded()
                        // change view height for smooth animation
                        // change constraint to fix in place
                        self.logInButton.frame.origin.y = self.view.frame.height - self.logInButton.frame.height - rect.height - 8
                        self.logInToBottomConstraint.constant = rect.height + 8
                })
            } else if notification.name == UIResponder.keyboardWillHideNotification {
                UIView.animate(
                    withDuration: 0.25,
                    delay: 0,
                    options: .curveEaseInOut,
                    animations: {
                        self.view.layoutIfNeeded()
                        self.logInButton.frame.origin.y = self.view.frame.height - self.logInButton.frame.height - 34
                        self.logInToBottomConstraint.constant = 34
                })
            }
        }
    }
}

extension EmailLogInViewController: UITextFieldDelegate {
    
    // Calls whenever text field changes
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let textField = textField as! LogInTextField
        
        textField.updatedText = textField.text ?? ""
        
        // Create and store update text
        textField.updatedText.removeSubrange(range)
        textField.updatedText.insert(contentsOf: string, at: range.location)

        textField.addOrRemovePlaceholderLabel()
        
        return true
    }
    
    // Go to next text field or next page after hitting return
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let textField = textField as! LogInTextField
        let textFieldArrayIndex = textFieldArray.firstIndex(of: textField)
        
//        if textField.isValid() {
        if textFieldArrayIndex == textFieldArray.count - 1 {
            // Home page!
            goToHomePage()
        } else {
            // Go to next text field
            textFieldArray[textFieldArrayIndex! + 1].becomeFirstResponder()
        }
        return true
    }
}

extension EmailLogInViewController: BudgetedUserLogInDelegate {
    func alertOrSendToHomeScreen(isValidUser: Bool) {
        DispatchQueue.main.async {
            if isValidUser {
                // Send to home screen
                self.performSegue(withIdentifier: "EmailLoginToHomeSegue", sender: self)
            } else {
                // Alert
                let alert = UIAlertController(title: "Invalid user", message: "Incorrect email or password. Please try again to continue.", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default))
                self.present(alert, animated: true)
            }
        }
    }
}
