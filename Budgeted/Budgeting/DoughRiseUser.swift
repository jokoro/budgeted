//import UIKit
import Alamofire
import SwiftyJSON

fileprivate extension String {
    static let apiURL = "http://production.eba-sqyithp3.us-west-2.elasticbeanstalk.com"
    
    static func id() throws -> String {
        guard let id = DoughRiseUser.user._id else {
            throw DoughRiseUser.AuthError.invalidId
        }
        return id
    }
    
    static func token() throws -> String {
        guard let token = DoughRiseUser.user.token else {
            throw DoughRiseUser.AuthError.invalidToken
        }
        return token
    }

//    static let newUserUrlExtension = "/register"
//    static let googleRegistrationUrlExtension = "/register/google"
//    static let facebookRegistrationUrlExtension = "/register/facebook"
//    static let appleRegistrationUrlExtension = "/register/apple"
    static let newUserUrlExtension = "/newUser"
    static let googleRegistrationUrlExtension = "/newgoogleuser"
    static let facebookRegistrationUrlExtension = "/newfacebookuser"
    static let appleRegistrationUrlExtension = "/newappleuser"
    
    static let loginUrlExtension = "/login"
    static let loginWithGoogleUrlExtension = "/loginGoogle"
    static let updatePasswordUrlExtension = "/password"
    static let sendPasswordResetEmailUrlExtension = "/password/email"
    static let inviteFriendsUrlExtension = ""
    static let
        getUserByIDUrlExtension = idUrlExtensionFunc(stringBfr: "/users/get/")
    static let updateAUserUrlExtension = "/users/update"
    static let uploadImageUrlExtension = ""
    static let getAllAccountsUrlExtension = "/accounts/all"
    static let createNewBudgetUrlExtension = "/budgets/create"
    static let editABudgetUrlExtension = "/budgets/edit"
    static let deleteABudgetUrlExtension = "/budgets/delete"
    static let getBudgetsByUserUrlExtension = "/budgets/all"
    static let getOneBudgetUserUrlExtension = idUrlExtensionFunc(stringBfr: "/budgets/get/")

    static let addItemToBudgetUrlExtension = "/budgets/budget-item"
    static let addAnExpenseToBudgetItemUrlExtension = idUrlExtensionFunc(stringBfr: "/budgets/item/", stringAft: "/expense")
    
    typealias IdUrlExtensionClosure = () throws -> String
    static func idUrlExtensionFunc(stringBfr: String, stringAft: String = "") -> IdUrlExtensionClosure {
        let idUrlExtensionClosure: IdUrlExtensionClosure = {
            return stringBfr + (try .id()) + stringAft
        }
        return idUrlExtensionClosure
    }
    
    func replaceByInserting(_ stringToInsert: String, at index: Int) -> String {
        var stringToChange = self
        stringToChange.insert(contentsOf: stringToInsert, at: stringToChange.index(stringToChange.startIndex, offsetBy: index))
        return stringToChange
    }
    
    static let kUserId = "id"
    static let kUserToken = "token"
}

fileprivate extension HTTPMethod {
    static let registerMethod = HTTPMethod.post
    static let loginMethod = HTTPMethod.post
    static let passwordResetEmailMethod = HTTPMethod.post
    static let updateMethod = HTTPMethod.put
    static let getUserMethod = HTTPMethod.get
    static let getAccountMethod = HTTPMethod.get

    static let createBudgetMethod = HTTPMethod.post
    static let editBudgetMethod = HTTPMethod.put
    static let getBudgetMethod = HTTPMethod.get
    static let deleteBudgetMethod = HTTPMethod.delete
}

fileprivate extension HTTPHeaders {
    typealias HeadersClosure = () throws -> HTTPHeaders
    static let tokenHeader: HeadersClosure = {
        return HTTPHeaders(["Authorization": try .token()]) //, "Content-Type": "application/json"])
    }
}

enum UpdateableParameter: String {
    case firstName
    case lastName
    case email
    case password
    case birthdate
}

struct DoughRiseUserData: Codable {
    var success: Bool?
    var user: DoughRiseUser?
}

class DoughRiseUser: Codable {
    private init() {}
    static var user = DoughRiseUser()  // Singleton
    static let group = DispatchGroup()
    
    // register + get user
    var firstName: String?
    var lastName: String?
    var email: String?
    var password: String?

    // get user
    var _id: String? { // register: userId
        get { UserDefaults.standard.string(forKey: .kUserId) }
        set { UserDefaults.standard.set(newValue, forKey: .kUserId) }
    }
    var token: String? {
        get { UserDefaults.standard.string(forKey: .kUserToken) }
        set { UserDefaults.standard.set(newValue, forKey: .kUserToken) }
    }
    var createdAt: String?
    var updatedAt: String?
    var __v: Int?
    var country: String?
    var city: String?
    var birthdate: String?
        
    enum LoginError: Error {
        case userExists
        case invalidCredentials
    }
    
    enum AuthError: Error {
        case invalidId
        case invalidToken
    }
    
    typealias SuccessHandler = ((Data?) throws -> Void)?
    typealias FailureHandler = ((Error) -> Void)?
    typealias Completion = (() -> Void)?
    typealias CompletionWithLoginError = (LoginError) -> ()
    
    private func registerUser(_ data: Data?, completionWithError: CompletionWithLoginError) {
        let json = JSON(data)
        _id = json["userId"].stringValue
        token = json["token"].stringValue

        print("id:", _id)
        print("token:", token)

        switch json["message"].stringValue {
        case "User already exists":
            completionWithError(.userExists)
            DoughRiseUser.group.leave()
        case "Email and/or Password incorrect":
            completionWithError(.invalidCredentials)
            DoughRiseUser.group.leave()
        case "Token successfully generated":
            do { try getUserByID { DoughRiseUser.group.leave() } }
            catch { print(error.localizedDescription) } // invalid id, log in 1st
        default:
            DoughRiseUser.group.leave()
        }
    }

    func newUser(firstName: String, lastName: String, email: String, password: String, completionWithError: @escaping CompletionWithLoginError) {
        let newUserParameters = ["firstName": firstName,
                                 "lastName": lastName,
                                 "email": email,
                                 "password": password]
        DoughRiseUser.group.enter()
        // New User
        DoughRiseUser.sendRequest(urlExtension: .newUserUrlExtension,
                                  method: .registerMethod,
                                  parameters: newUserParameters) { data in
            self.registerUser(data, completionWithError: completionWithError)
        }
    }

    func googleRegistration(googleId: String, email: String, givenName: String, familyName: String, completionWithError: @escaping CompletionWithLoginError) {
        let googleRegistrationParameters = ["googleId": googleId,
                                            "email": email,
                                            "givenName": givenName,
                                            "familyName": familyName]
        DoughRiseUser.group.enter()
        // Facebook Registration
        DoughRiseUser.sendRequest(urlExtension: .googleRegistrationUrlExtension,
                                  method: .registerMethod,
                                  parameters: googleRegistrationParameters) { data in
            self.registerUser(data, completionWithError: completionWithError)
        }
    }

    func facebookRegistration(facebookId: String, email: String, givenName: String, familyName: String, completionWithError: @escaping CompletionWithLoginError) {
        let facebookRegistrationParameters = ["facebookId": facebookId,
                                              "email": email,
                                              "givenName": givenName,
                                              "familyName": familyName]
        DoughRiseUser.group.enter()

        // Google Registration
        DoughRiseUser.sendRequest(urlExtension: .facebookRegistrationUrlExtension,
                                  method: .registerMethod,
                                  parameters: facebookRegistrationParameters) { data in
            self.registerUser(data, completionWithError: completionWithError)
        }
    }

    func appleRegistration(appleId: String, email: String, givenName: String, familyName: String, completionWithError: @escaping CompletionWithLoginError) {
        let appleRegistrationParameters = ["appleId": appleId,
                                            "email": email,
                                            "givenName": givenName,
                                            "familyName": familyName]
        DoughRiseUser.group.enter()

        // Apple Registration
        DoughRiseUser.sendRequest(urlExtension: .appleRegistrationUrlExtension,
                                  method: .registerMethod,
                                  parameters: appleRegistrationParameters) { data in
            self.registerUser(data, completionWithError: completionWithError)
        }
    }

    func login(email: String, password: String, completionWithError: @escaping CompletionWithLoginError) {
        let loginParameters = ["email": email,
                               "password": password] //Login(email: "John@demo.com", password: "ABC123")
        DoughRiseUser.group.enter()

        // Login
        DoughRiseUser.sendRequest(urlExtension: .loginUrlExtension,
                                  method: .loginMethod,
                                  parameters: loginParameters) { data in
            self.registerUser(data, completionWithError: completionWithError)
        }
    }

    func loginWithGoogle(googleId: String, completionWithError: @escaping CompletionWithLoginError) {
        let loginWithGoogleParameters = ["googleId": googleId]
        DoughRiseUser.group.enter()

        // Login With Google
        DoughRiseUser.sendRequest(urlExtension: .loginWithGoogleUrlExtension,
                                  method: .loginMethod,
                                  parameters: loginWithGoogleParameters) { data in
            self.registerUser(data, completionWithError: completionWithError)
        }
    }

//    {
//        func updatePassword(email: String, password: String) {
//            let updatePasswordParameters = ["email": email,
//                                            "password": password]
//
//            // Update Password
//            DoughRiseUser.sendRequest(urlExtension: .updatePasswordUrlExtension,
//                        method: .updateMethod,
//                        parameters: updatePasswordParameters,
//                        headers: try .tokenHeader())
//        }
//
//        func sendPasswordResetEmail(email: String) {
//            let sendPasswordResetEmailParameters = ["email": email]
//
//            // Send Password Reset Email
//            DoughRiseUser.sendRequest(urlExtension: .sendPasswordResetEmailUrlExtension,
//                        method: .passwordResetEmailMethod,
//                        parameters: sendPasswordResetEmailParameters,
//                        headers: try .tokenHeader())
//        }
//    }

    func getUserByID(completion: (() -> Void)? = nil) throws {
        // Get User By ID
        DoughRiseUser.sendRequest(urlExtension: try .getUserByIDUrlExtension(),
                                  method: .getUserMethod) { data in
            guard let data = data else { return }
            self.parseJSON(data: data)
        } completion: {
            completion?()
        }
    }
//{
//
//        func updateAUser(parameters: [UpdateableParameter: Any]) {
//            var updateAUserParameters: Parameters = [:]
//            parameters.forEach { updateAUserParameters["\($0)"] = $1 }
//
//            // New User
//            DoughRiseUser.sendRequest(urlExtension: .updateAUserUrlExtension,
//                        method: .updateMethod,
//                        parameters: updateAUserParameters,
//                        headers: try .tokenHeader())
//        }
//
//        func getAllAccounts() {
//            // Create new Budget
//            DoughRiseUser.sendRequest(urlExtension: .getAllAccountsUrlExtension,
//                        method: .getAccountMethod,
//                        headers: try .tokenHeader())
//        }
//
//        func createNewBudget(name: String, amount: Double, startDate: String, frequency: String) {
//            let createNewBudgetParameters: Parameters = ["name": name,
//                                                         "amount": amount,
//                                                         "startDate": startDate,
//                                                         "frequency": frequency]
//
//            // Create new Budget
//            DoughRiseUser.sendRequest(urlExtension: .createNewBudgetUrlExtension,
//                        method: .createBudgetMethod,
//                        parameters: createNewBudgetParameters,
//                        headers: try .tokenHeader())
//        }
//
//        func editABudget(name: String, budgetId: String) {
//            let editABudgetParameters: Parameters = ["name": name,
//                                                     "budgetId": budgetId]
//
//            // Edit a budget
//            DoughRiseUser.sendRequest(urlExtension: .editABudgetUrlExtension,
//                        method: .editBudgetMethod,
//                        parameters: editABudgetParameters,
//                        headers: try .tokenHeader())
//        }
//
//        func deleteABudget(budgetId: String) {
//            let deleteABudgetParameters: Parameters = ["budgetId": budgetId]
//
//            // Delete a Budget
//            DoughRiseUser.sendRequest(urlExtension: .deleteABudgetUrlExtension,
//                        method: .deleteBudgetMethod,
//                        parameters: deleteABudgetParameters,
//                        headers: try .tokenHeader())
//        }
//
//        func getBudgetsByUser() {
//            // Get Budgets by User
//            DoughRiseUser.sendRequest(urlExtension: .getBudgetsByUserUrlExtension,
//                        method: .getBudgetMethod,
//                        headers: try .tokenHeader())
//        }
//
//        func getOneBudget() throws {
//            // Get One Budget
//            DoughRiseUser.sendRequest(urlExtension: try .getOneBudgetUserUrlExtension(),
//                        method: .getBudgetMethod,
//                        headers: try .tokenHeader())
//        }
//
//        func addItemToBudget(budgetId: String, name: String, amount: Double, settings: Parameters, notes: String) {
//            let addItemToBudgetParameters: Parameters = ["budgetId": budgetId,
//                                                         "name": name,
//                                                         "amount": amount,
//                                                         "settings": settings,
//                                                         "notes": notes]
//
//            // Add Item to Budget
//            DoughRiseUser.sendRequest(urlExtension: .addItemToBudgetUrlExtension,
//                        method: .editBudgetMethod,
//                        parameters: addItemToBudgetParameters,
//                        headers: try .tokenHeader())
//        }
//
//        func addAnExpenseToBudgetItem(category: String, amount: Double, payee: String, date: String) throws {
//            let addAnExpenseToBudgetItemParameters: Parameters = ["category": category,
//                                                                  "amount": amount,
//                                                                  "payee": payee,
//                                                                  "date": date]
//
//            // Add an expense to budget item
//            DoughRiseUser.sendRequest(urlExtension: try .addAnExpenseToBudgetItemUrlExtension(),
//                        method: .editBudgetMethod,
//                        parameters: addAnExpenseToBudgetItemParameters,
//                        headers: try .tokenHeader())
//        }
//}

    func copy(fromGetUser original: DoughRiseUser) {
        firstName = original.firstName
        lastName = original.lastName
        email = original.email
        password = original.password
        _id = original._id
        createdAt = original.createdAt
        updatedAt = original.updatedAt
        __v = original.__v
        country = original.country
        city = original.city
        birthdate = original.birthdate
    }

    private static func sendRequest(urlExtension: String,
                                    method: HTTPMethod,
                                    parameters: Parameters? = nil,
                                    headers: HTTPHeaders? = nil,
                                    successHandler: SuccessHandler = nil,
                                    failureHandler: FailureHandler = nil,
                                    completion: Completion = nil) rethrows {
        AF.request(.apiURL + urlExtension,
                   method: method,
                   parameters: parameters,
                   headers: headers
        ).response { response in
            switch response.result {
            case .success(let value):
                print("JSON: \(JSON(value))")
                do { try successHandler?(response.data) }
                catch {  }
            case .failure(let error):
                print("ERROR: \(error)")
                failureHandler?(error)
            }
            completion?()
        }
    }
    
    func parseJSON(data: Data) {
        var userData: DoughRiseUserData?
        do {
            userData = try JSONDecoder().decode(DoughRiseUserData.self, from: data)
        } catch {
            debugPrint("Error took place: \(error.localizedDescription)")
        }
        if let user = userData?.user {
            self.copy(fromGetUser: user)
        }
    }
}
