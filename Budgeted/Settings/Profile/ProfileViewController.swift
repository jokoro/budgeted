//
//  ProfileViewController.swift
//  Budgeted
//
//  Created by Jason Okoro on 8/20/20.
//  Copyright © 2020 Divercity. All rights reserved.
//

import UIKit

class ProfileViewController: GeneralSettingsViewController {

    @IBOutlet weak var editingButtonsStackView: UIStackView!
    
    @IBOutlet weak var firstNameButton: SettingsButton!
    @IBOutlet weak var lastNameButton: SettingsButton!
    @IBOutlet weak var emailButton: SettingsButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
                
        firstNameButton.extraText = currentUser.getFirstName()
        lastNameButton.extraText = currentUser.getLastName()
        emailButton.extraText = currentUser.getEmail()
                
        editingButtonsStackView.layoutIfNeeded()

        firstNameButton.extraTextLabel.text = currentUser.getFirstName()
        lastNameButton.extraTextLabel.text = currentUser.getLastName()
        emailButton.extraTextLabel.text = currentUser.getEmail()
    }
}
