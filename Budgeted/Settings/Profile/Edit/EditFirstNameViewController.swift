//
//  EditFirstNameViewController.swift
//  Budgeted
//
//  Created by Jason Okoro on 8/20/20.
//  Copyright © 2020 Divercity. All rights reserved.
//

import UIKit

class EditFirstNameViewController: EditViewController {
    
    @IBOutlet weak var editFirstNameTextField: SettingsTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        editFirstNameTextField.delegate = self
    }
    
    func storeFirstName() {
        // Store first name somewhere it can be accessed to show it on this page and on the profile page again
    }
}
