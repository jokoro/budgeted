//
//  EditViewController.swift
//  Budgeted
//
//  Created by Jason Okoro on 8/21/20.
//  Copyright © 2020 Divercity. All rights reserved.
//

import UIKit

class EditViewController: GeneralSettingsViewController {
    
    // Shouldn't use EditViewController unless has textfield directly in subviews
    var textField: SettingsTextField! {
        get {
            for subview in view.subviews {
                if type(of: subview).self == SettingsTextField.self {
                    return subview as? SettingsTextField
                }
            }
            return nil
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "back"), style: .plain, target: self, action: #selector(alertAboutDiscardingSignUpInfo))
        navigationItem.leftBarButtonItem?.imageInsets = UIEdgeInsets(top: 0, left: -8, bottom: 0, right: 0)
        navigationItem.leftBarButtonItem?.title = "Profile"
        
        textField.delegate = self
    }
    
    @objc
    func alertAboutDiscardingSignUpInfo() {
        if let textField = textField {
            
            if textField.text != "" {
                
                // Create the alert
                let alert = UIAlertController(
                    title: "Are you sure?",
                    message: "If you go back now, this field will remain unchanged.",
                    preferredStyle: .alert
                )

                // Add an action button, go back if "Go Back" clicked
                alert.addAction(UIAlertAction(
                    title: "Go Back",
                    style: .default,
                    handler: {_ in
                        self.navigationController?.popViewController(animated: true)
                }))
                
                // Add cancel button, stay if "Stay" clicked
                alert.addAction(UIAlertAction(title: "Stay", style: .cancel))
                
                // Show the alert
                present(alert, animated: true)
                return
            }
//            else {
//                // Store new value for text field
//                currentUser.setFirstName(givenName: textField.text)
//            }
        }
        // Go back if no info entered to discard
        navigationController?.popViewController(animated: true)
    }
}
