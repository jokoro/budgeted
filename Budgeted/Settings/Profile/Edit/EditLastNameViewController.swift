//
//  EditLastNameViewController.swift
//  Budgeted
//
//  Created by Jason Okoro on 8/20/20.
//  Copyright © 2020 Divercity. All rights reserved.
//

import UIKit

class EditLastNameViewController: EditViewController {
    
//    @IBOutlet override weak var textField: SettingsTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func storeLastName() {
        // Store last name somewhere it can be accessed to show it on this page and on the profile page again
    }
}
