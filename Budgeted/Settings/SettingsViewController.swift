//
//  SettingsViewController.swift
//  Budgeted
//
//  Created by Jason Okoro on 8/19/20.
//  Copyright © 2020 Divercity. All rights reserved.
//

import UIKit
import GoogleAPIClientForREST
import AppAuth
import GTMAppAuth

let kIssuer: String = "https://accounts.google.com";
let kClientID: String? = "309832586685-71sssisa4u3bemsfh5vpdstgs6e7f989.apps.googleusercontent.com";
let kRedirectURI: String = "com.googleusercontent.apps.309832586685-71sssisa4u3bemsfh5vpdstgs6e7f989:/oauth2redirect/google";
let kGTMAppAuthKeychainItemName = "DriveSample: Google Drive. GTMAppAuth."

class SettingsViewController: GeneralSettingsViewController {

    @IBOutlet weak var profileButton: SettingsButton!
    @IBOutlet weak var budgetsButton: SettingsButton!
    @IBOutlet weak var accountsButton: SettingsButton!
    @IBOutlet weak var inviteButton: SettingsButton!
    @IBOutlet weak var logOutButton: SettingsButton!
    
    let driveService = { () -> GTLRDriveService in
        let service = GTLRDriveService()
        service.shouldFetchNextPages = true
        service.isRetryEnabled = true
        return service
    }()

    let sheetsService = { () -> GTLRSheetsService in
        let service = GTLRSheetsService()
//        service.shouldFetchNextPages = true
//        service.isRetryEnabled = true
        return service
    }()

    var fileList: GTLRDrive_FileList?
    var fileListFetchError: NSError?
    var fileListTicket: GTLRServiceTicket?
    var driveSpreadSheets: [GTLRDrive_File]? = []
    var spreadsheet: GTLRSheets_Spreadsheet!


    override func viewDidLoad() {
        super.viewDidLoad()
        layoutButtons()
        
        let authorization = GTMAppAuthFetcherAuthorization(fromKeychainForName: kGTMAppAuthKeychainItemName)
        driveService.authorizer = authorization;
        sheetsService.authorizer = authorization;

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        profileButton.setUpProfile()
    }
    
    @IBAction func connectGoogleSheetsPressed(_ sender: Any) {
        let alert = UIAlertController(title: "Sign in to Google Sheets", message: "Please sign in to your Google account to link a Google Sheet.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel))
        alert.addAction(UIAlertAction(title: "Sign In", style: .default, handler: { action in
            self.getFileList()
        }))
        present(alert, animated: true)
    }
    
    func layoutButtons() {
        let buttonsArray = [profileButton, budgetsButton, accountsButton, inviteButton, logOutButton]
        
        for button in buttonsArray {
            button?.layoutSubviews()
        }
    }

    func signedInUsername() -> String? {
        // Get the email address of the signed-in user.
        let auth = driveService.authorizer
        let isSignedIn = auth?.canAuthorize
        if isSignedIn != nil && isSignedIn! {
            return auth?.userEmail;
        } else {
            return nil;
        }
    }

    func isSignedIn() -> Bool {
        let name = self.signedInUsername()
        return name != nil
    }

    
    func getFileList() {
        if !self.isSignedIn() {
            self.runSigninThenHandler { self.fetchFileList() }
        } else {
            self.fetchFileList()
      }
    }

    func runSigninThenHandler(_ completion: @escaping () -> Void) {

        guard let issuer = URL(string: kIssuer) else {
            logMessage("Error creating URL for : \(kIssuer)")
            return
        }

        logMessage("Fetching configuration for issuer: \(issuer)")

        // discovers endpoints
        OIDAuthorizationService.discoverConfiguration(forIssuer: issuer) { configuration, error in

            guard let config = configuration else {
                logMessage("Error retrieving discovery document: \(error?.localizedDescription ?? "DEFAULT_ERROR")")
//                self.setAuthState(nil)
                return
            }

            logMessage("Got configuration: \(config)")

            if let clientId = kClientID {

                guard let redirectURI = URL(string: kRedirectURI) else {
                    logMessage("Error creating URL for : \(kRedirectURI)")
                    return
                }

                guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
                    logMessage("Error accessing AppDelegate")
                    return
                }

                // builds authentication request
                let request = OIDAuthorizationRequest(configuration: config,
                                                      clientId: clientId,
                                                      clientSecret: nil,
                                                      scopes: [OIDScopeOpenID, OIDScopeProfile, kGTLRAuthScopeDrive, OIDScopeEmail],
                                                      redirectURL: redirectURI,
                                                      responseType: OIDResponseTypeCode,
                                                      additionalParameters: nil)

                // performs authentication request
                logMessage("Initiating authorization request with scope: \(request.scope ?? "DEFAULT_SCOPE")")

                appDelegate.currentAuthorizationFlow = OIDAuthState.authState(byPresenting: request, presenting: self) { authState, error in

                    logMessage("authState: \(String(describing: authState))")
                    
                    if let authState = authState {
//                        self.setAuthState(authState)
                        logMessage("Got authorization tokens. Access token: \(authState.lastTokenResponse?.accessToken ?? "DEFAULT_TOKEN")")
                        
                        // MARK: THESE STEPS ARE WHAT ACTUALLY LET ME USE APIS!!!
                        // would need to get authState from Sign In API in order to use Drive & Sheets APIs too
                        // would also need to set their scopes
                        // maybe it's not even possible to
                        let gtmAuthorization =
                            GTMAppAuthFetcherAuthorization(authState: authState)
                        self.driveService.authorizer = gtmAuthorization;
                        self.sheetsService.authorizer = gtmAuthorization;

                        // Serializes authorization to keychain in GTMAppAuth format.
                        GTMAppAuthFetcherAuthorization.save(gtmAuthorization, toKeychainForName: kGTMAppAuthKeychainItemName)
                        
                        completion()
                        
                    } else {
                        self.fileListFetchError = error as NSError?
                        logMessage("Authorization error: \(error?.localizedDescription ?? "DEFAULT_ERROR")")
//                        self.setAuthState(nil)
                    }
                }
            }
        }
    }
    
    func fetchFileList() {
        let query = GTLRDriveQuery_FilesList.query()
        query.fields = "kind,nextPageToken,files(mimeType,id,kind,name,webViewLink,thumbnailLink,trashed)";

        driveService.executeQuery(query) { (callbackTicket, fileList, callbackError) in
            
            self.fileList = fileList as? GTLRDrive_FileList;
//            self.fileListFetchError = callbackError as NSError?;
            self.fileListTicket = nil
            
            if let fileList = fileList {
                logMessage("fileList: \(fileList)")
                self.printSpreadSheetFileNames()
                
                
                // Test Spreadsheets API here!
                let id = "1r6VNA33GC4BhoC-RQA6YbeCIdhpL3fUAI7dQ0QgxcDI" // DOUGHRISE Milestone Planning Doc
                //(self.driveSpreadSheets?[0].identifier)!
                let valueRange = GTLRSheets_ValueRange()
                valueRange.majorDimension = kGTLRSheets_ValueRange_MajorDimension_Rows
                valueRange.range = "H3:H4"
                valueRange.values = [["This line was edited from the app!!! -Jason", 343434, true]]

                let query = GTLRSheetsQuery_SpreadsheetsValuesUpdate.query(withObject: valueRange, spreadsheetId: id, range: valueRange.range!)
                query.valueInputOption = kGTLRSheetsValueInputOptionUserEntered
                
//                sGet.query(withSpreadsheetId: id)
//                query.includeGridData = true
//                query.ranges = ["H4"]

//                query.fields = ""
                self.sheetsService.executeQuery(query){ (callbackTicket, response, callbackError) in
                    
                    if response != nil { // as? GTLRSheets_Spreadsheet {
//                        self.spreadsheet = spreadsheet
                        logMessage("response: \(String(describing: response))")
                        
//                        self.logMessage("sheets: \(String(describing: self.spreadsheet.sheets))")
                        
                        
                    } else {
                        logMessage("spreadsheet callbackError: \(String(describing: callbackError))")
                    }
                }
                
                
            } else {
                logMessage("drive callbackError: \(String(describing: callbackError))")
            }
        }
    }
    
    func printAllFileNames() {
        guard let files = fileList?.files else {
            logMessage("No files to display :(")
            return
        }
        
        for file in files {
            logMessage(fileTitleWithLabels(for: file)!)
        }
    }
    
    func printSpreadSheetFileNames() { //application/vnd.google-apps.spreadsheet
        guard let files = fileList?.files else {
            logMessage("No files to display :(")
            return
        }
        
        var num = 0
        for file in files {
            if file.mimeType == "application/vnd.google-apps.spreadsheet" {
                logMessage(num, fileTitleWithLabels(for: file)!)
                self.driveSpreadSheets?.append(file)
                num += 1
            }
        }
    }
    
    func fileTitleWithLabels(for file: GTLRDrive_File) -> NSString? {

        if let name = file.name {
            let title = NSMutableString(string: name)
            
            if let starred = file.starred?.boolValue, starred {
                title.append(" \u{2605}") // star character
            }
            if let trashed = file.trashed?.boolValue, trashed {
                title.append(" \u{2717}") // X character
            }
            if let viewersCanCopyContent = file.viewersCanCopyContent?.boolValue, viewersCanCopyContent {
                title.append(" \u{21DF}") // crossed down arrow
            }
            
            if let kind = file.mimeType  {
                title.append("** \(kind)")
            }
            if let id = file.identifier  { // USE THIS AS SPREADSHEETSID!!!
                title.append("** \(id)")
            }
            return title;
        }
        return nil
    }

}

func logMessage(_ text: Any...) {
    print("* \(text)")
}
