//
//  InviteFriendsViewController.swift
//  Budgeted
//
//  Created by Jason Okoro on 8/20/20.
//  Copyright © 2020 Divercity. All rights reserved.
//

import UIKit

class InviteFriendsViewController: UIViewController {
    
    @IBOutlet weak var copyLinkButton: RegularButton!
    var link = "budgeted.com/join/alex.p"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        copyLinkButton.setTitle(link, for: .normal)
    }
    
    @IBAction func shareButtonPressed(_ sender: Any) {
        let activityVC = UIActivityViewController(activityItems: [link], applicationActivities: nil)
        activityVC.popoverPresentationController?.sourceView = view
        present(activityVC, animated: true)
    }
}
