//
//  InviteContactsViewController.swift
//  Budgeted
//
//  Created by Jason Okoro on 8/28/20.
//  Copyright © 2020 Divercity. All rights reserved.
//

import UIKit
import Contacts
//import ContactsUI
import MessageUI

class InviteContactsViewController: UITableViewController {
    let contactStore = CNContactStore()
    var contacts = [CNContact]()
    var phoneNumber: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadContacts()
    }
}

// Contacts methods
extension InviteContactsViewController {
    func loadContacts() {
        switch CNContactStore.authorizationStatus(for: .contacts) {
        case .authorized:
            contacts = findContacts()
//            printContacts(contacts)
        case .notDetermined:
            contactStore.requestAccess(for: .contacts) { (success, error) in
                guard success && error == nil else {
                    return
                }
                self.contacts = self.findContacts()
//                self.printContacts(self.contacts)
            }
        default:
            logMessage("Not handled")
        }
    }
    
    func findContacts() -> [CNContact] {
        let keysToFetch =     [CNContactFormatter.descriptorForRequiredKeys(for: .fullName), CNContactPhoneNumbersKey as NSString] //, CNContactImageDataKey, CNContactEmailAddressesKey, CNContactUrlAddressesKey, CNContactNoteKey, CNContactPhoneNumbersKey, CNContactPostalAddressesKey]

         let fetchRequest = CNContactFetchRequest(keysToFetch: keysToFetch)
         fetchRequest.unifyResults = true
        
         do {
            try contactStore.enumerateContacts(with: fetchRequest) { (contact, stop) -> Void in
                
                if contact.givenName != "" && ![nil, []].contains(contact.phoneNumbers) {
                    self.contacts.append(contact)
                }
             }
            contacts.sort { $0.givenName < $1.givenName }
         }
         catch let error as NSError {
             logMessage(error.localizedDescription)
         }

         return contacts

    }
    
    func printContacts(_ contacts: [CNContact]) {
        for contact in contacts {
            logMessage(contact)
        }
    }
}

// UITableViewDelegate methods
extension InviteContactsViewController {
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
}

// UITableViewDataSource methods
extension InviteContactsViewController {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contacts.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let contact = contacts[indexPath.row]
        
        let tableViewCell = InviteContactsTableViewCell()
        tableViewCell.setUp(presentingDelegate: self, contact: contact)
        
        return tableViewCell
    }

}

// MFMessageComposeViewControllerDelegate methods
extension InviteContactsViewController: MFMessageComposeViewControllerDelegate {
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        self.dismiss(animated: true)
    }
}

// Invite Button Pressed method
extension InviteContactsViewController {
    func inviteButtonPressed(phoneNumber: String) {
        if (MFMessageComposeViewController.canSendText()) {
            let controller = MFMessageComposeViewController()
            controller.body = "Join DoughRise today!"
            controller.recipients = [phoneNumber]
            controller.messageComposeDelegate = self
            present(controller, animated: true)
        }
    }
}

