//
//  DoughRiseTabBarController.swift
//  Budgeted
//
//  Created by Jason Okoro on 8/20/20.
//  Copyright © 2020 Divercity. All rights reserved.
//

import UIKit

class DoughRiseTabBarController: UITabBarController {
    let tabBarItemTextColor = UIColor(red: 0.487, green: 0.631, blue: 0.95, alpha: 1)
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        currentUser.isLoggedIn = true
        
        logMessage("token:", currentUser.token ?? "nil")
        
        let info = BudgetedUserLoggedInInfo(
                                            userId: currentUser._id!,
                                            token: currentUser.token!,
                                            isLoggedIn: currentUser.isLoggedIn!)
        do {
            let encodedData = try NSKeyedArchiver.archivedData(withRootObject: info, requiringSecureCoding: true)
            defaults.set(encodedData, forKey: kUserLoggedInInfo)
        } catch {
            
        }        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        for viewController in viewControllers! {
            viewController.tabBarItem.setTitleTextAttributes([.foregroundColor : tabBarItemTextColor], for: .normal)
        }
    }
}
