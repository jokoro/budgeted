//
//  GeneralSettingsViewController.swift
//  Budgeted
//
//  Created by Jason Okoro on 8/20/20.
//  Copyright © 2020 Divercity. All rights reserved.
//

import UIKit

class GeneralSettingsViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        view.setBackgroundColor(to: UIColor(red: 0.898, green: 0.898, blue: 0.898, alpha: 1).cgColor)
    }
}

extension GeneralSettingsViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let textField = textField as! SettingsTextField
        if textField.shouldNotResign() {
            let noTextAlert = UIAlertController(title: "No text entered", message: "Must enter text before continuing", preferredStyle: .alert)
            noTextAlert.addAction(UIAlertAction(title: "OK", style: .default) { alert in
                textField.becomeFirstResponder()
            })
            present(noTextAlert, animated: true)
        } else {
            // Save and dissmiss view controller
            if let _ = self as? EditFirstNameViewController {
                currentUser.setFirstName(givenName: textField.text)
                currentUser.updateUser(name: "firstName", value: textField.text!)
            } else if let _ = self as? EditLastNameViewController {
                currentUser.setLastName(familyName: textField.text)
                currentUser.updateUser(name: "lastName", value: textField.text!)
            } else if let _ = self as? EditEmailViewController {
                currentUser.setEmail(email: textField.text)
                currentUser.updateUser(name: "email", value: textField.text!)
            }
        }
        
        navigationController?.popViewController(animated: true)
        return true
    }
}
