//
//  ViewControllerHierarchy.swift
//  Budgeted
//
//  Created by Jason Okoro on 8/12/20.
//  Copyright © 2020 Divercity. All rights reserved.
//

import UIKit

// Instantiate all view controllers in the app here so can keep track of them from anywhere
let getStartedViewController = instantiateViewController(withId: "GetStartedViewController") as! GetStartedViewController
let signUpViewController = instantiateViewController(withId: "SignUpViewController") as! SignUpViewController
let logInViewController = instantiateViewController(withId: "LogInViewController") as! LogInViewController
let emailSignUpViewController = instantiateViewController(withId: "EmailSignUpViewController") as! EmailSignUpViewController
let emailLogInViewController = instantiateViewController(withId: "EmailLogInViewController") as! EmailLogInViewController

// Instantiate a view controller
private func instantiateViewController(withId id: String) -> UIViewController {
    return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: id)
}
